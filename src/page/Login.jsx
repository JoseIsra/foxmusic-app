import React  from 'react'
import './Login.css'
import logofoxbel from '../assets/foxbel-music.png'


const APP_ID = '473682';
const REDIRECT_URI = 'http://localhost:3000';
export const Login = () => {
    return (
        <div className="login">
            <div className="login__content">
            <img src={logofoxbel} alt="logo" />
            <a href={`https://connect.deezer.com/oauth/auth.php?app_id=${APP_ID}&redirect_uri=${REDIRECT_URI}&response_type=token&perms=basic_access,email`}>"Iniciar Sesión" 😆</a>
            </div>
        </div>
    )
}
