import { Grid } from '@material-ui/core'
import React, { useEffect } from 'react'
import { Sidebar, Body } from '../index'
import { PlayMusicBar } from '../playbar/PlayMusicBar'
import { api } from '../../apiconfig/requests';
import useStyles from './styles';
import { useDataLayerValue } from '../../datacontext/DataLayer'

export const Main = () => {
    const classes = useStyles();
    const [, dispatch ] = useDataLayerValue();

    useEffect(()=> {
        api.getData('blinding lights')
        .then(response => {
            dispatch({
                type:'SET__DEEZER__DATA',
                payload:response
            })
        }).catch(error => console.error(error))
    },[dispatch]);

    return (
        
        
        <Grid container direction="row" className={classes.root}>
                
                <Grid item sm={2} md={3} lg={3}>
                    <Sidebar />
                </Grid>
                <Grid item sm={10} md={9} lg={9}>
                    <Body />
                </Grid>
            <Grid item  sm={4} md={6} lg={12} className={classes.play}>
            <PlayMusicBar />
            </Grid>
            
        </Grid>
    )
}
