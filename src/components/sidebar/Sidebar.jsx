import React from 'react'
import { Box, Typography } from '@material-ui/core';
import foxbelLogo from '../../assets/foxbel-music.png'
import useStyles from './styles'

export const Sidebar = () => {
    const classes = useStyles();
    


    return (
        <div className={classes.root}>
            <img className={classes.image}
                src={foxbelLogo}
                alt="logofoxbel"
            />
            <div className={classes.options}>
                <Typography variant="h6">Mi librería</Typography>
                <Typography variant="caption" component="p">Recientes</Typography>
                <Typography variant="caption" component="p">Artistas</Typography>
                <Typography variant="caption" component="p">Álbums</Typography>
                <Typography variant="caption" component="p">Canciones</Typography>
                <Typography variant="caption" component="p">Estaciones</Typography>
            </div>
            <Box className={classes.options}>
                <Typography variant="h6">Playlist</Typography>
                <Typography variant="caption" component="p">Metal</Typography>
                <Typography variant="caption" component="p">Para bailar</Typography>
                <Typography variant="caption" component="p">Rock 90s</Typography>
                <Typography variant="caption" component="p">Baladas</Typography>
            </Box>
        </div>
    )
}
