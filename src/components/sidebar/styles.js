import {makeStyles } from '@material-ui/core/styles';




export default makeStyles((theme)=>({
    root: {
        display:'flex',
        flexDirection:'column',
        alignItems:'center',
        backgroundColor:'#662323',
        height:'100vh'
    },
    image: {
        marginTop:'1rem',
        width: '250px',
        height: '59px',
    },
    options: {
        position:'relative',
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        marginTop:'2rem',
        '& > h6 ': {
            color:'#FFFFFF',
            fontFamily:'Quicksand',
            fontWeight:'700',
            lineHeight:'27px',
            fontSize:'22px'
        },
        '& > p': {
            color: 'rgba(255,255,255,.5)',
            fontSize:'16px',
            lineHeight:'20px',
            cursor:'pointer',
            '&:hover': {
                color:'#E86060'
            }
        }
    }
    

    
}));