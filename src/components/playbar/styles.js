import {makeStyles } from '@material-ui/core/styles'

export default makeStyles(()=>({
root: {
    height:'100px',
    position:'fixed',
    width:'100%',
    bottom:'0',
}
}));