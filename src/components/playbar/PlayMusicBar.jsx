import React from 'react'
import useStyles from './styles';
import Iframe from 'react-iframe'
import { useDataLayerValue } from '../../datacontext/DataLayer';
export const PlayMusicBar = () => {
    const classes = useStyles();
    const [{track}] = useDataLayerValue();
    return (
        <div className={classes.root}>
        <Iframe id="473682"
        url={`https://www.deezer.com/plugins/player?autoplay=false&playlist=false&type=tracks&id=${track}app_id=473682`}
        width='100%'
        styles={{backgroundColor:'black'}}
        />
        </div>
    )
}
