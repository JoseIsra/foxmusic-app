import {makeStyles } from '@material-ui/core/styles'

export default makeStyles(()=>({
    root: {
        display:'flex',
        marginTop:'2rem',
        position:'relative',
        marginLeft:'1rem',
        width:'90%',
        
    },
    bodyContainer: {
        height:'80vh',
        overflow:'auto'
    },
    albumPicture: {
        width:'300px',
        objectFit: 'contain'
    },
    details: {
        display:'flex',
        flexDirection: 'column',
        background: 'linear-gradient(0deg, rgba(167, 0, 0, 0.7), rgba(167, 0, 0, 0.7))',
        mixBlendMode: 'normal',
        width:'100%',
        zIndex:'999',
        
        '&  h5': {
            color:'#FFFFFF',
            fontFamily:'Quicksand',
            fontSize:'1.5rem',
            lineHeight: '27px',
            fontWeight:'bold'
        }
    },
    moreDetails: {
        display: 'flex',
        marginTop:'10px',
        alignItems:'center',
        marginBottom:'1rem',
        '& > *': {  
        fontFamily: 'Quicksand'
        },
        ' & > p:nth-child(1)': {
            color:'white',
            marginRight:'10px',
            fontWeight: 'normal',
            fontStyle: 'normal',
            fontSize: '1.2rem',
            lineHeight:'17px'
        },
        ' & > p:nth-child(2)': {
            color:'white',
            fontWeight: 'normal',
            fontStyle: 'normal',
            fontSize: '20px',
            lineHeight:'12px'
        }
    
    },
    buttons: {
        marginTop:'1rem',
        display:'flex',
        justifyContent:'space-around',
        alignItems: 'center',
        width:'500px',
        '& > button': {
            borderRadius:'100px',
            padding:'15px',
            width:'137px',
            mixBlendMode:'normal',
            outline:'none',
            cursor:'pointer'
        },
        '& >button:nth-child(1)': {
            color:'#FFFFFF',
            fontSize:'1rem',
            backgroundColor: '#E86060',
            border:'none'
        },
        '& > button:nth-child(2)': {
            border: '1px solid #EB5757',
            backgroundColor: 'transparent',
            color:'#E86060'
        }
    },
    backgroundPicture: {
        position:'absolute',
        top:'0',
        right:'0',
        zIndex:'1',
        width:'400px',
        objectFit:'contain'
    },
    description: {
        color:'white',
        fontFamily:'Quicksand',
        fontSize:'1rem',
        fontWeight:'normal',
        lineHeight: '20px'
    },
    grid: {
        display:'flex',
        marginTop:'1rem',
        flexWrap:'wrap',
        justifyContent:'space-around',
        width:'90%',
        marginLeft:'1rem',
    }



}));