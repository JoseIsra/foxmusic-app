import {makeStyles } from '@material-ui/core/styles'

export default makeStyles(()=>({
    root: {
        paddingTop:'10px',
    },
    leftSide: {
        ' & > form ': {
            position:'relative',
            width:'100%',
            display:'flex',
        },
        '&  button' : {
            border:'none',
            position:'absolute',
            backgroundColor:'transparent',
            right:'10px',
            top:'0.2rem',
            cursor:'pointer',
            
        },
        '& input': {
            width:'100%',
            fontSize:'1rem',
            borderRadius:'100px',
            border:'1px solid #828282',
            backgroundColor:'#FFFFFF',
            height:'2rem',
            paddingLeft:'10px',
            outline:'none',
            paddingRight:'30px'
        }
    },
    rightSide: {
        display: 'flex',
        justifyContent:'space-evenly',
        aligntItems:'center',
    }

}));