import React, { useState } from 'react'
import { Grid, Typography } from '@material-ui/core'
import PersonIcon from '@material-ui/icons/Person';
import SearchIcon from '@material-ui/icons/Search';
import { api } from '../../../apiconfig/requests';
import useStyles from './styles';
import { useDataLayerValue } from '../../../datacontext/DataLayer';

export const Topbar = () => {
    const classes = useStyles();
    const [input , setInput ] = useState('');
    const [,dispatch] = useDataLayerValue();

    const handleChange = (event) => {
        setInput(event.target.value);
    }

    const searchTerm = async(e)=> {
        e.preventDefault();
        try {
            const response = await api.getData(input);
            dispatch({
                type:'SET__DEEZER__DATA',
                payload:response
            })
            setInput('');
        } catch (error) {
            console.error(error);
        }
    };  

    return (
        <Grid container justify="space-around"  className={classes.root} alignItems="center">
            <Grid item md={6} lg={8} className={classes.leftSide} >
                <form onSubmit={searchTerm}>
                <input placeholder="Buscar" value={input} type="text" onChange={handleChange} />
                <button type="submit" >
                <SearchIcon/>
                </button>
                </form>
            </Grid>
            <Grid item md={6} lg={3} className={classes.rightSide}>
                <PersonIcon fontSize="small" style={{color:'#E86060'}}/>
                <Typography variant="caption">Israel D.</Typography>
            </Grid>
        </Grid>
    )
}
