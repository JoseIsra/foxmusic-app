import React, {useEffect, useState } from 'react';
import { useDataLayerValue } from '../../datacontext/DataLayer';
import { Topbar , Track } from '../index';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import { Card, CardContent, CardMedia, Typography } from '@material-ui/core';
import { api } from '../../apiconfig/requests';
import useStyles from './styles';

export const Body = () => {
    const classes = useStyles();
    const [, dispatch ] = useDataLayerValue();
    const [{deezerData}] = useDataLayerValue();
    const [fans, setFans ] = useState(null);

    useEffect(() => {
        if(deezerData?.length > 0) {
            api.getArtist(deezerData[0].artist.id)
            .then(response => {
                setFans(response.nb_fan)
            })
            .catch(error => console.error(error))
            dispatch({
                type: 'PLAY__TRACK',
                payload: deezerData[0].id
            })
        }
        
    }, [deezerData,dispatch])

    const selectTrack = (idTrack) =>{
        dispatch({
            type: 'PLAY__TRACK',
            payload: idTrack
        })
    }


    return (
        <div >
                <Topbar />
                
            {deezerData?.length > 0 ? (
                <div className={classes.bodyContainer}>
            <Card className={classes.root}>
                <CardMedia component="img"
                src={deezerData[0].album.cover}
                title="logo"
                className={classes.albumPicture}
                />  
                
                <div className={classes.details}>
                    <CardContent >
                    <Typography variant="h5">{deezerData[0].artist.name}</Typography>
                    <div className={classes.moreDetails}>
                    <p>Lo mejor de {deezerData[0].artist.name}</p>
                    <p >Seguidores {fans}</p>
                    </div>
                    <p className={classes.description} >Breve descripción del artista </p> 
                    <div className={classes.buttons}>
                        <button onClick={()=>selectTrack(deezerData[0].id)}>Reproducir</button>
                        <button>Seguir</button>
                        <MoreHorizIcon fontSize="large" style={{color:'white'}}/>
                    </div>
                    </CardContent>
                </div>
                    <img  src={deezerData[0].artist.picture} className={classes.backgroundPicture} alt="figura 2"/>
                    
                </Card>
                
                <div className={classes.grid}>
                    {deezerData.map(item => (
                        <Track key={item.id}
                        id={item.id} 
                        artist={item.artist.name} 
                        artistPicture={item.artist.picture}
                        trackTitle={item.title}
                        />
                    ))}
                </div>
                </div>
                ) : <div>
                    <p>NO hay resultados</p>
                </div>
                } 
                

        </div>
    )
}
