import React from 'react'
import useStyles from './styles'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import PlayArrowIcon from '@material-ui/icons/PlayArrow'
import { Card ,CardMedia, CardContent } from '@material-ui/core'
import { useDataLayerValue } from '../../../datacontext/DataLayer'



export const Track = ({id, artist, artistPicture, trackTitle}) => {
    const classes = useStyles();
    const [, dispatch ] = useDataLayerValue();
    const playSelectedTrack = (id) => {
        dispatch({
            type:'PLAY__TRACK',
            payload:id
        })
    }


    return (
        <div className={classes.trackBox}>
            <Card className={classes.root}>
                <CardMedia component="img"
                className={classes.albumPicture}
                image={artistPicture}
                title="logo"
                />
                <div>
                    <CardContent className={classes.trackInfo}>
                        <p>{trackTitle}</p>
                        <p>{artist}</p>
                    </CardContent>
                </div>
            </Card>
            <PlayArrowIcon fontSize="large"
            onClick={()=> playSelectedTrack(id)}
            style={{color:'white'}} className={classes.playIcon} />
            <MoreVertIcon fontSize="large" style={{color:'white'}} className={classes.dostIcon} />
        </div>
    )
}
