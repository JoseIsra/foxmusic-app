import {makeStyles } from '@material-ui/core/styles'

export default makeStyles(() => ({
    root: {
        display:'flex',
        flexDirection:'column',
    },
    trackBox: {
        position:'relative',
        width:'200px',
        margin:'0.5rem auto'
    },
    albumPicture: {
        width:'100%',
        objectFit:'contain'
    },
    trackInfo: {
        '& > p:nth-child(1)': {
            color:'#555555',
            fontSize:'20px',
            marginBottom:'0.3rem',
            fontFamilY:'Quicksand',
            fontWeight:'bold',
            fontStyle:'normal',
            lineHeight:'17px',
        },
        '& > p:nth-child(2)': {
            color:'#828282',
            fontSize:'18px',
            fontFamilY:'Quicksand',
            fontWeight:'normal',
            fontStyle:'normal',
            lineHeight:'15px'
        }
    },
    playIcon: {
        position:'absolute',
        left:'50%',
        top:'50%',
        transform: 'translate(-50%, -50%)',
        cursor:'pointer',
        '& :hover':{
            color:'black',
        }
    },
    dostIcon: {
        position:'absolute',
        top: '0.5rem',
        right:'0.5rem',
    }




}));