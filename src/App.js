import React, { useEffect } from 'react';
import { Container } from '@material-ui/core';
import { Main } from './components'
import{ Login } from './page/Login'
import { useDataLayerValue } from './datacontext/DataLayer';
import { getMyAmazingToken } from './apiconfig/deezer-extract-token';



function App() {
  const [{token},dispatch ] = useDataLayerValue();

  useEffect(()=> {
    const tokenHash = getMyAmazingToken();
    window.location.hash="";
    const myToken = tokenHash.access_token;
    console.log(myToken)
    if(myToken){
      dispatch({
        type:'SAVE__TOKEN',
        payload:myToken      
      })
  
    }
  },[dispatch])

  return (
      <>
      {token ? 
      <Container maxWidth="xl"  style={{height:'100%'}}  disableGutters={true} className="app">
      <Main />
    </Container>
    : <Login />
      }
    </>
  );
}

export default App;
