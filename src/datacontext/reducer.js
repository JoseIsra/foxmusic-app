
export const initialState = {
    deezerData: null,
    track:106506510,
    user:null,
    token:''
};

export const reducer = (state, action)=> {
    switch(action.type) {
        case 'SET__DEEZER__DATA': {
            return {
                ...state, 
                deezerData:action.payload
            }
        }
        case 'PLAY__TRACK': {
            return {
                ...state,
                track: action.payload
            }
        }
        case 'SAVE__TOKEN': {
            return {
                ...state,
                token: action.payload
            }
        }
        default: return state
    }
};

