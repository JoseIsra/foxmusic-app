

export const getMyAmazingToken = () => {
    return window
    .location
    .hash
    .substring(1)
    .split('&')
    .reduce((theToken,item)=>{
        let urlFragments = item.split('=');
        theToken[urlFragments[0]]=decodeURIComponent(urlFragments[1]);
        return theToken;
    },{})
}