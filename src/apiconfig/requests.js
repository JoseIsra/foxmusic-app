

const getData = async (input) => {
    const result = await fetch (`/search/track/?q=${input}&index=0&limit=10`);
    const json = await result.json();
    return json.data;
};

const getArtist = async (idArtist) => {
    const result = await fetch(`/artist/${idArtist}`);
    const json = await result.json();
    return json;

};


export const api = {
        getData,
        getArtist,
}